from datetime import datetime
from itertools import count
from typing import Iterable

from qosbench.evaluation.benchmarker import Benchmarker
from qosbench.evaluation.minibrass_executor import MiniBrassExecutor
from qosbench.evaluation.minizinc_executor import MiniZincExecutor
from qosbench.evaluation.qosagg_executor import QosAggExecutor
from qosbench.evaluation.reporting import CsvBenchmarkReporter
from qosbench.qosagg.qosagg_generator import QosAggGeneratorConfig


def get_configs(
        tpp: int = 3,
        tpc: int = 3,
        parallel: int = 3,
        choices: int = 3,
        iterations: int = 2,
        ppt: int = 3,
        attributes: int = 2,
) -> Iterable[QosAggGeneratorConfig]:
    return [QosAggGeneratorConfig(tpp, tpc, parallel, choices, iterations, ppt, attributes, seed) for seed in range(10)]


Experiment = Iterable[Iterable[QosAggGeneratorConfig]]


def get_values(start: int = 1) -> Iterable[int]:
    value = start
    while True:
        yield value
        value += 1 + (value // 10)


def get_experiments() -> Iterable[Experiment]:
    def exp1() -> Experiment:
        for value in get_values():
            yield get_configs(10, 0, 1, 0, 10, value, 1)

    def exp2a() -> Experiment:
        for value in get_values():
            yield get_configs(value, 0, 1, 0, 10, 2, 1)

    def exp2b() -> Experiment:
        for value in get_values():
            yield get_configs(10, 0, 1, 0, value, 2, 1)

    def exp3() -> Experiment:
        for value in get_values():
            yield get_configs(0, 1, 0, value, 1, 2, 1)

    def exp4() -> Experiment:
        for value in get_values():
            yield get_configs(1, 0, 1, 0, 10, 10, value)

    def exp5() -> Experiment:
        for value in get_values():
            yield get_configs(1, 0, value, 0, 10, 2, 1)

    def all2_ppt() -> Experiment:
        for value in get_values():
            yield get_configs(2, 2, 2, 2, 2, value, 2)

    def d2_tpp_prl() -> Experiment:
        for i in count(1):
            yield [QosAggGeneratorConfig(i, 0, j, 0, 10, 2, 1, seed) for j in range(1, i + 1) for seed in range(10)] +\
                  [QosAggGeneratorConfig(j, 0, i, 0, 10, 2, 1, seed) for j in range(1, i) for seed in range(10)]

    def d2_ppt_chc() -> Experiment:
        for i in count(1):
            yield [QosAggGeneratorConfig(0, 1, 0, j, 1, i, 1, seed) for j in range(1, i + 1) for seed in range(10)] +\
                  [QosAggGeneratorConfig(0, 1, 0, i, 1, j, 1, seed) for j in range(1, i) for seed in range(10)]

    return [
        # exp1(),
        # exp2a(),
        # exp2b(),
        # exp3(),
        # exp4(),
        # exp5(),
        # all2_ppt(),
        # d2_tpp_prl(),
        d2_ppt_chc()
    ]


qosagg_exec = ["qosagg"]
mzn_exec = ["minizinc",
            "--solver", "gurobi",
            "--gurobi-dll", "/home/elias/miniBrass/gurobi912/linux64/lib/libgurobi91.so",
            "--parallel", "7"]
mbr_exec = ["minibrass", "-M-p=7",
            "--solver", "gurobi",
            "-M--gurobi-dll=/home/elias/miniBrass/gurobi912/linux64/lib/libgurobi91.so"]
timeout = 15

if __name__ == '__main__':
    separate_flattening = True
    with open(datetime.now().strftime("%Y-%m-%d_%H-%M.csv"), "a", buffering=1) as csv_file:
        reporter = CsvBenchmarkReporter(csv_file, separate_flattening)
        benchmarker = Benchmarker(reporter=reporter,
                                  qag_exec=QosAggExecutor(qosagg_exec, timeout),
                                  mzn_exec=MiniZincExecutor(mzn_exec, timeout, separate_flattening),
                                  mbr_exec=MiniBrassExecutor(mbr_exec, timeout))
        for experiment in get_experiments():
            try:
                for configs in experiment:
                    all_timed_out = benchmarker.benchmark(configs)
                    if all_timed_out:
                        break
            except KeyboardInterrupt:
                continue
