# Mapping Experiment to CSV

* Exp. 1 (ppt): [2021-11-02_16-50.csv](2021-11-02_16-50.csv)
* Exp. 2a (tpp): [2021-10-28_12-32.csv](2021-10-28_12-32.csv)
* Exp. 2b (iterations): [2021-10-28_12-32.csv](2021-10-28_12-32.csv)
* Exp. 3 (choices): [2021-10-28_12-32.csv](2021-10-28_12-32.csv)
* Exp. 4 (attributes): [2021-10-28_12-32.csv](2021-10-28_12-32.csv)
* Exp. 5 (parallel): [2021-10-28_15-02.csv](2021-10-28_15-02.csv)
* tpp vs. parallel: [2021-11-03_15-59.csv](2021-11-03_15-59.csv)
* ppt vs. choices: [2021-11-05_15-56.csv](2021-11-05_15-56.csv)

