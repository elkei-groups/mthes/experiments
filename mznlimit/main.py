import itertools
import pathlib
import subprocess
import sys
import tempfile


def run():
    with tempfile.NamedTemporaryFile("wt", suffix=".mzn", dir=pathlib.Path.cwd()) as big_file:
        print(f"Using file {big_file.name}")
        big_file.write(f"solve minimize 0;\n")
        big_file.write("enum ENUM1 = { e1 };\n")
        big_file.write("enum ENUM2 = { e2 };\n")
        big_file.write("float: v = 0;\n\n")

        last_success = None
        last_change_at = None
        last_err = None
        last_out = None
        constraints = 0
        for i in itertools.count(0):
            for _ in range(i - constraints):
                big_file.write("constraint v < 10;\n")
            constraints = i
            big_file.flush()
            result = subprocess.run(args=["/home/elias/miniBrass/miniZinc/bin/minizinc", "-c", big_file.name],
                                    stderr=subprocess.PIPE, stdout=subprocess.PIPE, text=True)
            if result.stderr or result.returncode:
                if result.stderr and last_err != result.stderr:
                    print(result.stderr, file=sys.stderr)
                    last_err = result.stderr
                if result.stdout and last_out != result.stdout:
                    print(result.stdout, file=sys.stderr)
                    last_out = result.stdout
                success = False
            else:
                success = True
            if last_success != success:
                print(f"Starting with {i}: {success}")
                last_success = success
                last_change_at = i
            elif (i - last_change_at) % 500 == 0:
                print(f"Reached {i}")


if __name__ == '__main__':
    try:
        run()
    except KeyboardInterrupt:
        pass
