qosagg skateboards.qag skateboards_wf.mzn 
qos2mzn skateboards.qrk skateboards_rank.mzn --polyrank $HOME/Repositories/qoschecker-polyranker/ranker-tool.py

alias minibrass='PATH="$PATH:$HOME/Repositories/qoschecker-polyranker:$HOME/miniBrass/miniZinc/bin:$HOME/Repositories/mthes/qos2poly" $HOME/Repositories/minibrass/source-code/java/minibrass'
alias minizinc='$HOME/miniBrass/miniZinc/bin/minizinc'

if [ "$1" = "--no-mbr" ]; then
	shift 1
	minizinc skateboards.mzn $@
else
	minibrass skateboards.mbr skateboards.mzn -M-p=7 --solver gurobi -M--gurobi-dll=/home/elias/miniBrass/gurobi912/linux64/lib/libgurobi91.so $@
	rm log*.txt skateboards_0.mzn
fi