set of int: PROV = 1..3;
set of int: REQ = 1..57;
set of float: RANK = 0.0..1.0;
var PROV: prov;

array[REQ, PROV] of var RANK: ranks = ContractRanks {
	provisions: [
		% provision 1 is really bad
		14 ≤ c ≤ 16 ∧ 04 ≤ p ≤ 06,
		% provision 2 has c/p of 1 but is slow
		04 ≤ c ≤ 06 ∧ 04 ≤ p ≤ 06,
		% provision 3 has alos c/p of 1 but is fast
		14 ≤ c ≤ 16 ∧ 14 ≤ p ≤ 16,
	];
	requirements: [ 
		% "encoded" soft constraints:
		%   best cost/performance ration
		%   LEX
		%   highest performance

		% c/p ≤ 1/9
		c ≤ 03 ∧ p ≥ 27,
		c ≤ 02 ∧ p ≥ 18,
		c ≤ 01 ∧ p ≥ 09,
		% c/p ≤ 1/4
		c ≤ 07 ∧ p ≥ 28,
		c ≤ 06 ∧ p ≥ 24,
		c ≤ 05 ∧ p ≥ 20,
		c ≤ 04 ∧ p ≥ 16,
		c ≤ 03 ∧ p ≥ 12,
		c ≤ 02 ∧ p ≥ 08,
		c ≤ 01 ∧ p ≥ 04,
		% c/p ≤ 3/7
		c ≤ 12 ∧ p ≥ 28,
		c ≤ 09 ∧ p ≥ 21,
		c ≤ 06 ∧ p ≥ 14,
		c ≤ 03 ∧ p ≥ 07,
		% c/p ≤ 2/3
		c ≤ 18 ∧ p ≥ 27,
		c ≤ 16 ∧ p ≥ 24,
		c ≤ 14 ∧ p ≥ 21,
		c ≤ 12 ∧ p ≥ 18,
		c ≤ 10 ∧ p ≥ 15,
		c ≤ 08 ∧ p ≥ 12,
		c ≤ 06 ∧ p ≥ 09,
		c ≤ 04 ∧ p ≥ 06,
		c ≤ 02 ∧ p ≥ 03,
		% c/p ≤ 1
		c ≤ 28 ∧ p ≥ 28,
		c ≤ 25 ∧ p ≥ 25,
		c ≤ 22 ∧ p ≥ 22,
		c ≤ 19 ∧ p ≥ 19,
		c ≤ 16 ∧ p ≥ 16,
		c ≤ 13 ∧ p ≥ 13,
		c ≤ 10 ∧ p ≥ 10,
		c ≤ 08 ∧ p ≥ 08,
		c ≤ 06 ∧ p ≥ 06,
		c ≤ 04 ∧ p ≥ 04,
		c ≤ 02 ∧ p ≥ 02,
		% c/p ≤ 3/2
		c ≤ 27 ∧ p ≥ 18,
		c ≤ 24 ∧ p ≥ 16,
		c ≤ 21 ∧ p ≥ 14,
		c ≤ 18 ∧ p ≥ 12,
		c ≤ 15 ∧ p ≥ 10,
		c ≤ 12 ∧ p ≥ 08,
		c ≤ 09 ∧ p ≥ 06,
		c ≤ 06 ∧ p ≥ 04,
		c ≤ 03 ∧ p ≥ 02,
		% c/p ≤ 7/3
		c ≤ 28 ∧ p ≥ 12,
		c ≤ 21 ∧ p ≥ 09,
		c ≤ 14 ∧ p ≥ 06,
		c ≤ 07 ∧ p ≥ 03,
		% c/p ≤ 4
		c ≤ 28 ∧ p ≥ 07,
		c ≤ 24 ∧ p ≥ 06,
		c ≤ 20 ∧ p ≥ 05,
		c ≤ 16 ∧ p ≥ 04,
		c ≤ 12 ∧ p ≥ 03,
		c ≤ 08 ∧ p ≥ 02,
		c ≤ 04 ∧ p ≥ 01,
		% c/p ≤ 9
		c ≤ 27 ∧ p ≥ 03,
		c ≤ 18 ∧ p ≥ 02,
		c ≤ 09 ∧ p ≥ 01,
	];
};

array[REQ] of var RANK: prov_ranks = [ ranks[i, prov] | i in index_set(REQ) ];
% find first requirement that gives req a rank of at least 90%
var REQ: first_acceptable_req;
constraint prov_ranks[first_acceptable_req] >= 0.9;
constraint forall(i in REQ where i < first_acceptable_req)(prov_ranks[i] < 0.9);
output [ "Provision: \(prov)\nFirst Req: \(first_acceptable_req)\nRanks: \(prov_ranks)" ];
solve minimize first_acceptable_req;