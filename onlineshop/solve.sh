PATH="$PATH:$HOME/Repositories/qoschecker-polyranker:$HOME/miniBrass/miniZinc/bin:$HOME/Repositories/mthes/qos2poly:$HOME/Repositories/minibrass/source-code/java/"

gurobi_dll="/home/elias/miniBrass/gurobi912/linux64/lib/libgurobi91.so"
alias minizinc='minizinc --solver gurobi --gurobi-dll $gurobi_dll -p 7'
#alias minizinc='minizinc -p 7'
alias qoratom='qoratom --polyrank ranker-tool.py --minizinc minizinc'
alias minibrass='minibrass -M-p=7 --solver gurobi -M--gurobi-dll=$gurobi_dll'
#alias minibrass='minibrass -M-p=7'

qoratom shop.qrk shop_rank.mzn
qosagg shop.qag shop_wf.mzn
if [ "$1" != "--no-mbr" ]; then
	minibrass shop.mbr shop.mzn $1
	rm log*.txt
else
	minizinc shop.mzn $2
fi
